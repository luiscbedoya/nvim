local opts = { noremap = true, silent = true }
local fopts = { noremap = false, silent = true }
local keymap = vim.keymap.set
keymap('n', '<Space>', '', opts)
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '
-- USER COMMANDS  *************************************************************************
-- -- ***** SAVE AND CLOSE
keymap('n', '<C-s>', '<cmd>w<CR>', opts) -- save buffer
keymap('n', '<leader>q', '<cmd>q<CR>', fopts) -- quit  window
keymap('n', '<leader>x', '<cmd>bd<CR>', fopts) -- close current window
keymap('n', '<leader>Q', '<cmd>w<CR><cmd>q<CR>', opts) -- save and close window
-- -- *** INDENT
keymap('v', 'ñ', '<gv', opts)
keymap('v', '<A-ñ>', '>gv', opts)
keymap('n', '<A-ñ>', '>>', opts)
keymap('n', 'ñ', '<<', opts)
-- -- *** ENTER NORMAL MODE
keymap('i', 'jk', '<ESC>', opts)
-- keymap('v', 'jk', '<ESC>',opts)
-- keymap('v', 'jk', '<ESC>',opts)
-- -- -- *** RESIZE WINDOWS
keymap('n', '<C-A-k>', '<cmd> resize +2<CR>', opts)
keymap('n', '<C-A-j>', '<cmd> resize -2<CR>', opts)
keymap('n', '<C-A-l>', '<cmd> vertical resize -2<CR>', opts)
keymap('n', '<C-A-h>', '<cmd> vertical resize +2<CR>', opts)
-- *** MOVE BETWEEN WINDOWS
keymap({ 'n', 't' }, '<C-Left>', '<C-w>h', opts)
keymap({ 'n', 't' }, '<C-Down>', '<C-w>j', opts)
keymap({ 'n', 't' }, '<C-Up>', '<C-w>k', opts)
keymap({ 'n', 't' }, '<C-Right>', '<C-w>l', opts)
-- ** MOVE BETWEEN TABS
keymap({ 'n', 't' }, '<C-i>', ':tabnext<CR>', opts)
-- -- *** SPLIT WINDWS
-- keymap('n', '<leader>vs', ':vsplit ')
-- keymap('n', '<leader>ds', ':split ')
-- -- *** MOVE ENTIRE LINE
keymap('n', '<A-k>', ':m-2<CR>', opts)
keymap('n', '<A-j>', ':m+1<CR>', opts)
-- -- ** COMMAND MODE
keymap('n', '<C-Space>', ':')
-- *** FORMATING
-- keymap('n', '<C-f>', '<cmd>lua vim.lsp.buf.format({async = true})<cr>', fopts)
-- -- *** OTHERS
keymap('n', 'v', 'V', opts)
-- ** DELETE POSITION
keymap('n', '<A-0>', 'd$', opts)
-- SEARCH BREAK
keymap('n', 't', '%', opts)
keymap('v', 't', '%', opts)
-- keymap('n', 'm', '#', opts)
-- keymap('v', 'm', '#', opts)
-- EXTERNAL COMMANDS ****************************************************************************+*
-- Neogen
keymap('n', '<leader>n', ':Neogen<CR>', opts)
-- BufferLine
keymap('n', '<Tab>', ':BufferLineCycleNext<CR>', opts)
keymap('n', '<A-n>', ':BufferLineMovePrev<CR>', opts)
keymap('n', '<A-m>', ':BufferLineMoveNext<CR>', opts)
keymap('n', '<A-p>', ':BufferLineTogglePin<CR>', opts)
-- Telescope
keymap('n', '<leader>ff', '<cmd>Telescope find_files<cr>', opts)
keymap('n', '<leader>fg', '<cmd>Telescope live_grep<cr>', opts)
keymap('n', '<leader>fb', '<cmd>Telescope buffers<cr>', opts)
keymap('n', '<leader>fh', '<cmd>Telescope help_tags<cr>', opts)
keymap('n', '<leader>fr', '<cmd>Telescope oldfiles<cr>', opts)
keymap('n', '<leader>fp', '<cmd>Telescope projects<cr>', opts)
keymap('n', '<leader>fk', '<cmd>Telescope keymaps<cr>', opts)
-- Nvim-tree
keymap('n', '<C-n>', '<cmd>NvimTreeToggle<CR>', opts)
-- Rest
-- vim.api.nvim_create_autocmd('FileType', {
--   pattern = 'http',
--   callback = function()
--     local buff = tonumber(vim.fn.expand('<abuf>'), 10)
--     local rest_nvim = require('rest-nvim')
--     keymap('n', '<leader>rn', rest_nvim.run, { noremap = true, buffer = buff })
--     keymap('n', '<leader>rl', rest_nvim.last, { noremap = true, buffer = buff })
--     keymap('n', '<leader>rp', function()
--       rest_nvim.run(true)
--     end, { noremap = true, buffer = buff })
--   end,
-- })
-- Vim-dadbod-ui
keymap('n', '<leader><leader>db', ':tab DBUI<CR>', opts)
keymap('n', '<leader><leader>dc', ':DBUIClose<CR>', opts)
keymap('n', '<leader><leader>cd', ':DBUIAddConnection<CR>', opts)
-- nvim-dap
keymap('n', '<leader>dt', '<cmd>lua require"dap".toggle_breakpoint()<CR>', opts)
keymap('n', '<leader>dc', '<cmd>lua require"dap".continue()<CR>', opts)
keymap('n', '<leader>di', '<cmd>lua require"dap".step_into()<CR>', opts)
keymap('n', '<leader>do', '<cmd>lua require"dap".step_over()<CR>', opts)
-- nvim-dap-ui
keymap('n', '<leader>du', "<cmd>lua require'dapui'.toggle({reset=true})<CR>", opts)
-- lsp keymap
keymap('n', 'gd', '<cmd>Telescope lsp_definitions<CR>', opts)
keymap('n', 'gD', '<cmd>Telescope lsp_declarations<CR>', opts)
keymap('n', 'gI', '<cmd>Telescope lsp_implementations<CR>', opts)
keymap('n', 'gr', '<cmd>Telescope lsp_references<CR>', opts)
keymap('n', 'gl', '<cmd>lua vim.diagnostic.open_float()<CR>', opts)
keymap('n', 'ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
keymap('n', '<leader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
keymap('n', 'H', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
-- keymap('i', '<leader>w', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
keymap('n', '<leader>sh', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
keymap('n', '[d', '<cmd>lua vim.diagnostic.goto_prev({ border = "rounded" })<CR>', opts)
keymap('n', ']d', '<cmd>lua vim.diagnostic.goto_next({ border = "rounded" })<CR>', opts)
keymap('n', '<leader><leader>q', '<cmd>lua vim.diagnostic.setloclist()<CR>', opts)
keymap('n', '<leader>le', '<cmd>lua vim.lsp.codelens.refresh()<CR>', opts)
-- jvm
keymap('n', '<leader>jo', "<Cmd>lua require'jdtls'.organize_imports()<CR>", opts)
keymap('n', '<leader>jv', "<Cmd>lua require('jdtls').extract_variable()<CR>", opts)
keymap('n', '<leader>jc', "<Cmd>lua require('jdtls').extract_constant()<CR>", opts)
keymap('n', '<leader>jm', "<Cmd>lua require('jdtls').extract_method(true)<CR>", opts)
keymap('n', '<leader>tm', "<Cmd>lua require'jdtls'.test_nearest_method()<CR>", opts)
keymap('n', '<leader>tc', "<Cmd>lua require'jdtls'.test_class()<CR>", opts)
keymap('n', '<leader>ju', '<Cmd>JdtUpdateConfig<CR>', opts)
keymap('n', '<F4>', '<Cmd>JdtCompile full<CR>', opts)
--zenmode
keymap('n', '<leader>z', ':ZenMode<cr>', opts)
-- inc rename
keymap('n', '<leader>rn', ':IncRename ', opts)
