require('zen-mode').setup({
  window = {
    backdrop = 1,
    width = 250,
    height = 1,
  },
  plugins = {
    options = {
      enabled = true,
      ruler = false,
      showcmd = false,
      laststatus = 0,
    },
    twilight = { enabled = true },
    gitsigns = { enabled = true },
    kitty = {
      enabled = true,
      font = '+4',
    },
  },
})
