local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)
plugins = {
  --**********************************************************
  --UI
  -- {
  --   'RRethy/base16-nvim',
  --   name = 'base16',
  --   lazy = false,
  --   priority = 1000,
  -- },
  {
    'olivercederborg/poimandres.nvim',
    name = 'poimandres',
    lazy = false,
    priority = 1000,
    config = function()
      require('plugins.poimandres')
    end,
  },
  -- {
  --   'EdenEast/nightfox.nvim',
  --   name = 'nightfox',
  --   lazy = false,
  --   priority = 1000,
  --   config = function()
  --     require('plugins.nightfox')
  --   end,
  -- },
  { 'nvim-lua/plenary.nvim' },
  { 'hiphish/rainbow-delimiters.nvim' },
  { 'kyazdani42/nvim-web-devicons' },
  { 'JoosepAlviste/nvim-ts-context-commentstring' },
  {
    'lukas-reineke/indent-blankline.nvim',
    main = 'ibl',
    config = function()
      require('ibl').setup()
    end,
  },
  {
    'norcalli/nvim-colorizer.lua',
    event = 'BufRead',
    config = function()
      require('colorizer').setup()
    end,
  },
  {
    'terrortylor/nvim-comment',
    lazy = false,
    config = function()
      require('plugins.comment')
    end,
  },
  {
    'windwp/nvim-autopairs',
    lazy = false,
    config = function()
      require('plugins.autopairs')
    end,
  },
  {
    'utilyre/barbecue.nvim',
    event = { 'BufEnter', 'FileType' },
    config = function()
      require('plugins.barbecue')
    end,
  },
  {
    'SmiteshP/nvim-navic',
    event = 'FileType',
  },
  {
    'RRethy/vim-illuminate',
    config = function()
      require('plugins.iluminate')
    end,
  },
  -- ************************************************************
  -- NEOGEN
  {
    'danymat/neogen',
    cmd = { 'Neogen' },
    config = function()
      require('neogen').setup({ snippet_engine = 'luasnip' })
    end,
  },
  -- ************************************************************
  -- SURROUND
  {
    'tpope/vim-surround',
  },
  -- ************************************************************
  -- INC_RENAME
  {
    'smjonas/inc-rename.nvim',
  },
  -- ************************************************************
  -- **** CMDLINE *****************
  {
    'rcarriga/nvim-notify',
    config = function()
      require('plugins.notify')
    end,
  },
  {
    'folke/noice.nvim',
    dependencies = {
      'MunifTanjim/nui.nvim',
    },
    event = 'BufWinEnter',
    config = function()
      require('plugins.noice')
    end,
  },
  -- ************************************************************
  -- -- LUALINE
  {
    'nvim-lualine/lualine.nvim',
    lazy = false,
    config = function()
      require('plugins.lualine')
    end,
  },
  -- ************************************************************
  -- BUFFERLINE
  {
    'akinsho/bufferline.nvim',
    -- lazy = false,
    config = function()
      require('plugins.bufferline')
    end,
  },
  -- ************************************************************
  -- TREESITTER
  {
    'nvim-treesitter/nvim-treesitter',
    lazy = false,
    config = function()
      require('plugins.treesitter')
    end,
  },
  --************************************************************
  --TELESCOPE
  {
    'nvim-telescope/telescope.nvim',
    config = function()
      require('plugins.telescope')
    end,
  },
  --************************************************************
  --NVIMTREE
  {
    'kyazdani42/nvim-tree.lua',
    config = function()
      require('plugins.nvimtree')
    end,
  },
  --************************************************************
  -- **** ZENMODE *****************
  {
    'folke/zen-mode.nvim',
    config = function()
      require('plugins.zenmode')
    end,
  },
  --************************************************************
  -- **** twilight *****************
  {
    'folke/twilight.nvim',
    cmd = { 'Twilight' },
    config = true,
  },
  --************************************************************
  -- **** MINIANIMATE *****************
  -- {
  --   'echasnovski/mini.animate',
  --   config = function()
  --     require('mini.animate').setup()
  --   end,
  -- },
  --************************************************************
  --TOGGLETERM
  {
    'akinsho/toggleterm.nvim',
    version = '*',
    config = function()
      require('plugins.toggleterm')
    end,
  },
  --************************************************************
  --DAP
  {
    'mfussenegger/nvim-dap',
    event = 'VeryLazy',
    config = function()
      require('plugins.dap')
    end,
  },
  {
    'rcarriga/nvim-dap-ui',
    dependencies = {
      'nvim-neotest/nvim-nio',
    },
    after = 'nvim-dap',
    config = function()
      require('plugins.dapui')
    end,
  },
  {
    'mfussenegger/nvim-dap-python',
    ft = 'py',
    after = 'nvim-dap',
    config = function()
      require('dap-python').setup('~/.local/share/nvim/mason/packages/debugpy/venv/bin/python')
    end,
  },
  --************************************************************+
  --GLOW
  {
    'ellisonleao/glow.nvim',
    md = 'Glow',
    ft = 'markdown',
    config = function()
      require('glow').setup({
        style = 'dark',
        width = 800,
      })
    end,
  },
  --************************************************************+
  --GIT
  {
    'lewis6991/gitsigns.nvim',
    cond = function()
      local git_dir = vim.fn.finddir('.git', '.;')
      return git_dir ~= ''
    end,
    config = function()
      require('plugins.git')
    end,
  },
  --************************************************************+
  --SNIPPETS
  {
    'L3MON4D3/LuaSnip',
    version = 'v2.*',
    build = 'make install_jsregexp',
  },
  { 'rafamadriz/friendly-snippets' },
  -- *************************************************************
  -- cmp
  { 'hrsh7th/nvim-cmp' },
  { 'hrsh7th/cmp-buffer' },
  { 'hrsh7th/cmp-path' },
  { 'hrsh7th/cmp-cmdline' },
  { 'saadparwaiz1/cmp_luasnip' },
  { 'hrsh7th/cmp-nvim-lsp' },
  { 'mfussenegger/nvim-jdtls' },
  --confrom
  { 'stevearc/conform.nvim' },
  -- lsp
  { 'neovim/nvim-lspconfig' },
  { 'nvimtools/none-ls.nvim' },
  { 'williamboman/mason-lspconfig.nvim' },
  { 'williamboman/mason.nvim' },
  { 'jayp0521/mason-nvim-dap.nvim' },
  { 'jayp0521/mason-null-ls.nvim' },
  -- *************************************************************
  -- DATABASE
  {
    'kristijanhusak/vim-dadbod-ui',
    dependencies = {
      'tpope/vim-dadbod',
      'kristijanhusak/vim-dadbod-completion',
      'tpope/vim-dotenv',
    },
    config = function()
      require('plugins.database')
    end,
  },
}
require('lazy').setup(plugins, opts)
