require('illuminate').configure({
  providers = {
    'lsp',
    'treesitter',
    'regex',
  },
  filetypes_denylist = {
    'help',
    'alpha',
    'neo-tree',
    'NvimTree',
    'Trouble',
    'lazy',
    'norg',
  },
})
