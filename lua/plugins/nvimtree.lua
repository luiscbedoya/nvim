local api = require('nvim-tree.api')
local icons = require('icons')

-- Replaces auto_close
local tree_cb = api.nvim_tree_callback
vim.api.nvim_create_autocmd('BufEnter', {
  nested = true,
  callback = function()
    if #vim.api.nvim_list_wins() == 1 and vim.api.nvim_buf_get_name(0):match('NvimTree_') ~= nil then
      vim.cmd('quit')
    end
  end,
})

require('nvim-tree').setup({
  filters = {
    dotfiles = false,
  },
  disable_netrw = true,
  hijack_netrw = true,
  hijack_cursor = true,
  hijack_unnamed_buffer_when_opening = false,
  sync_root_with_cwd = true,
  update_focused_file = {
    enable = true,
    update_root = false,
  },
  view = {
    adaptive_size = false,
    side = 'left',
    width = 30,
    preserve_window_proportions = true,
  },
  git = {
    enable = true,
    ignore = true,
  },
  filesystem_watchers = {
    enable = true,
  },
  actions = {
    open_file = {
      resize_window = true,
    },
  },
  renderer = {
    root_folder_label = false,
    highlight_git = true,
    highlight_opened_files = 'none',

    indent_markers = {
      enable = true,
    },

    icons = {
      show = {
        file = true,
        folder = true,
        folder_arrow = true,
        git = true,
      },
      glyphs = {
        default = icons.documents.File,
        symlink = icons.documents.Symlink,
        bookmark = icons.ui.Bookmark,
        modified = icons.git.Mod,
        folder = {
          arrow_closed = icons.ui.ArrowClosed,
          arrow_open = icons.ui.ArrowOpen,
          default = icons.documents.Folder,
          open = icons.documents.OpenFolder,
          empty = icons.documents.EmptyFolder,
          empty_open = icons.documents.EmptyFolderOpen,
          symlink = icons.documents.FolderSymlink,
          symlink_open = icons.documents.FolderSymlinkOpen,
        },
        git = {
          unstaged = icons.git.Mod,
          staged = icons.git.Staged,
          unmerged = icons.git.Unmerged,
          renamed = icons.git.Rename,
          untracked = icons.git.Untracked,
          deleted = icons.git.Remove,
          ignored = icons.git.Ignore,
        },
      },
    },
  },
})
