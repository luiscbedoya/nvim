require('gitsigns').setup({
  signcolumn = true,
  numhl = false,
  linehl = false,
  trouble = false,
  word_diff = false,
  watch_gitdir = {
    interval = 1000,
    follow_files = true,
  },
  signs = {
    add = { text = '│' },
    change = { text = '│' },
    delete = { text = '󰍵' },
    topdelete = { text = '‾' },
    changedelete = { text = '~' },
    untracked = { text = '│' },
  },
  attach_to_untracked = true,
  current_line_blame = true,
  current_line_blame_opts = {
    virt_text = true,
    virt_text_pos = 'eol',
    delay = 1000,
    ignore_whitespace = false,
  },
  current_line_blame_formatter_opts = {
    relative_time = false,
  },
  sign_priority = 6,
  update_debounce = 100,
  status_formatter = nil,
  max_file_length = 40000,
  preview_config = {

    border = 'single',
    style = 'minimal',
    relative = 'cursor',
    row = 0,
    col = 1,
  },
  yadm = {
    enable = false,
  },
})
