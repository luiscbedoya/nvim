local icons = require('icons')
require('bufferline').setup({
  highlights = {
    fill = { bg = '#0b0c0d' },
    buffer_selected = { bold = true },
    diagnostic_selected = { bold = true },
    info_selected = { bold = true },
    info_diagnostic_selected = { bold = true },
    warning_selected = { bold = true },
    warning_diagnostic_selected = { bold = true },
    error_selected = { bold = true },
    error_diagnostic_selected = { bold = true },
  },
  options = {
    separator_style = { '', '' },
    groups = {
      options = {
        toggle_hidden_on_enter = true,
      },
      items = {
        {
          name = 'Tests',
          highlight = { guisp = '#56B6C2', gui = 'underline' },
          priority = 2,
          icon = ' ',
          matcher = function(buf)
            return buf.name:match('http')
          end,
        },
        -- {
        --   name = 'Docs',
        --   highlight = { guisp = '#FFD700', gui = 'underline' },
        --   priority = 1,
        --   icon = '',
        --   matcher = function(buf)
        --     return buf.name:match('%f[%w]md%f[%W]') or buf.name:match('%f[%w]docx%f[%W]')
        --   end,
        -- },
        -- Puedes definir más grupos aquí...
      },
    },
    diagnostics = 'nvim_lsp',
    max_prefix_length = 8,
    -- diagnostics_indicator = function(count, level, diagnostics_dict, context)
    --   if context.buffer:current() then
    --     return ''
    --   end
    --   if level:match('error') then
    --     return ' ' .. icons.diagnostics.Error
    --   elseif level:match('warning') then
    --     return ' ' .. icons.diagnostics.Warning
    --   end
    --   return ''
    -- end,
    -- custom_filter = function(buf_number, buf_numbers)
    --   if vim.bo[buf_number].filetype ~= 'oil' then
    --     return true
    --   end
    -- end,
  },
})
